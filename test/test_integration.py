import os
import pytest
from app import app
from flask import url_for


@pytest.fixture
def client():
    app.config['TESTING'] = True
    app.config['SERVER_NAME'] = 'test'
    client = app.test_client()
    with app.app_context():
        pass
    app.app_context().push()
    yield client


def test_dinausaur(client):
    rv = client.get(url_for('dinausaure',slug='dilophosaurus'))
    assert 200 == rv.status_code
    assert b'Carnivore' in rv.data
