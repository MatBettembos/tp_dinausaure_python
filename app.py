from flask import Flask,render_template
import requests,random
app = Flask(__name__)

@app.route('/')
def index():
    response = requests.get('https://allosaurus.delahayeyourself.info/api/dinosaurs/')
    json_response = response.json()
    sum=0
    return render_template('list_dinausaure.html',json_response=json_response)


@app.route('/dinosaur/<slug>')
def dinausaure(slug='<slug>'):
    response = requests.get('https://allosaurus.delahayeyourself.info/api/dinosaurs/'+slug)
    response2 = requests.get('https://allosaurus.delahayeyourself.info/api/dinosaurs/')
    json_response = response.json()
    json_response2 = response2.json()
    json_top_rate=random.sample(json_response2,3)
    return render_template('dinausaure.html',json_response=json_response,json_top_rate=json_top_rate)
